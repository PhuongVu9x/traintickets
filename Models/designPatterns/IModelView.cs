﻿namespace DemoAspNetCore.Models.designPatterns
{
    public interface IModelView<T>
    {
        public T Get(T data);
        public ICollection<T> Gets();
        public ICollection<T> Search(T data);
    }
}
