﻿namespace DemoAspNetCore.Models.designPatterns
{
    public interface IModelCrud<T>
    {
        public string Insert(T data);
        public string Update(T data);
        public string ChangeStatus(T data);

        public bool IsExists(T data);
    }
}
