﻿using DemoAspNetCore.Models.Business;
using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.designPatterns
{
    public class ModelMaker
    {
        private static readonly Lazy<ModelMaker> lazy = new(() => new ModelMaker());
        public static ModelMaker Instance { get { return lazy.Value; } }

        private AdminMgr _adminMgr;
        private ClassMgr _classMgr;
        private CustomerMgr _customerMgr;
        private FareMgr _fareMgr;
        private ScheduleMgr _scheduleMgr;
        private StationMgr _stationMgr;
        private TrainMgr _trainMgr;
        private TicketMgr _ticketMgr;

        public ModelMaker()
        {
            _adminMgr = new AdminMgr();
            _classMgr = new ClassMgr();
            _customerMgr = new CustomerMgr();
            _fareMgr = new FareMgr();
            _scheduleMgr = new ScheduleMgr();
            _stationMgr = new StationMgr();
            _trainMgr = new TrainMgr();
            _ticketMgr = new TicketMgr();
        }

        #region Admin
        public string Insert(Admin data)
        {
            return _adminMgr.Insert(data);
        }

        public string Update(Admin data)
        {
            return _adminMgr.Update(data);
        }

        public string ChangeStatus(Admin data)
        {
            return _adminMgr.ChangeStatus(data);
        }

        public bool IsExists(Admin data)
        {
            return _adminMgr.IsExists(data);
        }

        public Admin Get(Admin data)
        {
            return _adminMgr.Get(data);
        }

        public ICollection<Admin> GetAdmins()
        {
            return _adminMgr.Gets();
        }

        public ICollection<Admin> Search(Admin data)
        {
            return _adminMgr.Search(data);
        }
        #endregion

        #region Class
        public string Insert(Class data)
        {
            return _classMgr.Insert(data);
        }

        public string Update(Class data)
        {
            return _classMgr.Update(data);
        }

        public bool IsExists(Class data)
        {
            return _classMgr.IsExists(data);
        }

        public Class Get(Class data)
        {
            return _classMgr.Get(data);
        }

        public string ChangeStatus(Class data)
        {
            return _classMgr.ChangeStatus(data);
        }

        public ICollection<Class> GetClasses() 
        {
            return _classMgr.Gets();
        }
        
        public ICollection<Class> Search(Class data)
        {
            return _classMgr.Search(data);
        }
        #endregion

        #region Customer
        public string Insert(Customer data)
        {
            return _customerMgr.Insert(data);
        }

        public string Update(Customer data)
        {
            return _customerMgr.Update(data);
        }

        public bool IsExists(Customer data)
        {
            return _customerMgr.IsExists(data);
        }

        public Customer Get(Customer data)
        {
            return _customerMgr.Get(data);
        }

        public string ChangeStatus(Customer data)
        {
            return _customerMgr.ChangeStatus(data);
        }

        public ICollection<Customer> GetCustomers()
        {
            return _customerMgr.Gets();
        }

        public ICollection<Customer> Search(Customer data)
        {
            return _customerMgr.Search(data);
        }
        public bool Check(Customer data)
        {
            return _customerMgr.IsExists(data);
        }

        #endregion

        #region Fare
        public string Insert(Fare data)
        {
            return _fareMgr.Insert(data);
        }

        public string Update(Fare data)
        {
            return _fareMgr.Update(data);
        }

        public bool IsExists(Fare data)
        {
            return _fareMgr.IsExists(data);
        }

        public Fare Get(Fare data)
        {
            return _fareMgr.Get(data);
        }

        public string ChangeStatus(Fare data)
        {
            return _fareMgr.ChangeStatus(data);
        }

        public ICollection<Fare> GetFares()
        {
            return _fareMgr.Gets();
        }

        public ICollection<Fare> Search(Fare data)
        {
            return _fareMgr.Search(data);
        }
        #endregion

        #region Schedule
        public string Insert(Schedule data)
        {
            return _scheduleMgr.Insert(data);
        }

        public string Update(Schedule data)
        {
            return _scheduleMgr.Update(data);
        }

        public bool IsExists(Schedule data)
        {
            return _scheduleMgr.IsExists(data);
        }

        public Schedule Get(Schedule data)
        {
            return _scheduleMgr.Get(data);
        }

        public string ChangeStatus(Schedule data)
        {
            return _scheduleMgr.ChangeStatus(data);
        }

        public ICollection<Schedule> GetSchedules()
        {
            return _scheduleMgr.Gets();
        }

        public ICollection<Schedule> Search(Schedule data)
        {
            return _scheduleMgr.Search(data);
        }
        #endregion

        #region Station
        public string Insert(Station data)
        {
            return _stationMgr.Insert(data);
        }

        public string Update(Station data)
        {
            return _stationMgr.Update(data);
        }

        public bool IsExists(Station data)
        {
            return _stationMgr.IsExists(data);
        }

        public string ChangeStatus(Station data)
        {
            return _stationMgr.ChangeStatus(data);
        }

        public Station Get(Station data)
        {
            return _stationMgr.Get(data);
        }

        public ICollection<Station> GetStations()
        {
            return _stationMgr.Gets();
        }

        public ICollection<Station> Search(Station data)
        {
            return _stationMgr.Search(data);
        }
        #endregion

        #region Ticket
        public string Insert(Ticket data)
        {
            return _ticketMgr.Insert(data);
        }

        public string Update(Ticket data)
        {
            return _ticketMgr.Update(data);
        }

        public bool IsExists(Ticket data)
        {
            return _ticketMgr.IsExists(data);
        }

        public Ticket Get(Ticket data)
        {
            return _ticketMgr.Get(data);
        }

        public string ChangeStatus(Ticket data)
        {
            return _ticketMgr.ChangeStatus(data);
        }

        public ICollection<Ticket> GetTickets()
        {
            return _ticketMgr.Gets();
        }

        public ICollection<Ticket> Search(Ticket data)
        {
            return _ticketMgr.Search(data);
        }

        public string Check(Ticket data)
        {
            return _ticketMgr.IsExists(data).ToString().ToLower();
        }
        #endregion

        #region Train
        public string Insert(Train data)
        {
            return _trainMgr.Insert(data);
        }

        public string Update(Train data)
        {
            return _trainMgr.Update(data);
        }

        public bool IsExists(Train data)
        {
            return _trainMgr.IsExists(data);
        }

        public string ChangeStatus(Train data)
        {
            return _trainMgr.ChangeStatus(data);
        }

        public Train Get(Train data)
        {
            return _trainMgr.Get(data);
        }

        public ICollection<Train> GetTrains()
        {
            return _trainMgr.Gets();
        }

        public ICollection<Train> Search(Train data)
        {
            return _trainMgr.Search(data);
        }
        #endregion
    }
}
