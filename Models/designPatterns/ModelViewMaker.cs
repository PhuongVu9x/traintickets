﻿using DemoAspNetCore.Models.Business;
using DemoAspNetCore.Models.entities;
using DemoAspNetCore.Models.modelViews;

namespace DemoAspNetCore.Models.designPatterns
{
    public class ModelViewMaker
    {
        private static readonly Lazy<ModelViewMaker> lazy = new(() => new ModelViewMaker());
        public static ModelViewMaker Instance { get { return lazy.Value; } }

        private AdminViewMgr _adminViewMgr;
        private StationViewMgr _stationViewMgr;
        private TrainViewMgr _trainViewMgr;
        private ClassViewMgr _classViewMgr;
        private FareViewMgr _fareViewMgr;
        private ScheduleViewMgr _scheduleViewMgr;
        private CustomerViewMgr _customerViewMgr;
        private TicketViewMgr _ticketViewMgr;

        public ModelViewMaker()
        {
            _adminViewMgr = new AdminViewMgr();
            _stationViewMgr = new StationViewMgr();
            _classViewMgr = new ClassViewMgr();
            _trainViewMgr = new TrainViewMgr();
            _fareViewMgr = new FareViewMgr();
            _scheduleViewMgr = new ScheduleViewMgr();
            _customerViewMgr = new CustomerViewMgr();
            _ticketViewMgr = new TicketViewMgr();
        }

        #region AdminView

        //public FareView Get(FareView data)
        //{
        //    return _fareViewMgr.Get(data);
        //}

        public ICollection<AdminView> GetAdmins()
        {
            return _adminViewMgr.Gets();
        }

        public ICollection<AdminView> Search(AdminView data)
        {
            return _adminViewMgr.Search(data);
        }
        #endregion

        #region StationView

        //public FareView Get(FareView data)
        //{
        //    return _fareViewMgr.Get(data);
        //}

        public ICollection<StationView> GetStations()
        {
            return _stationViewMgr.Gets();
        }

        public ICollection<StationView> Search(StationView data)
        {
            return _stationViewMgr.Search(data);
        }
        #endregion

        #region TrainView

        //public FareView Get(FareView data)
        //{
        //    return _fareViewMgr.Get(data);
        //}

        public ICollection<TrainView> GetTrains()
        {
            return _trainViewMgr.Gets();
        }

        public ICollection<TrainView> Search(TrainView data)
        {
            return _trainViewMgr.Search(data);
        }
        #endregion

        #region ClassView

        //public FareView Get(FareView data)
        //{
        //    return _fareViewMgr.Get(data);
        //}

        public ICollection<ClassView> GetClasses()
        {
            return _classViewMgr.Gets();
        }

        public ICollection<ClassView> Search(ClassView data)
        {
            return _classViewMgr.Search(data);
        }
        #endregion

        #region FareView

        public FareView Get(FareView data)
        {
            return _fareViewMgr.Get(data);
        }

        public ICollection<FareView> GetFares()
        {
            return _fareViewMgr.Gets();
        }

        public ICollection<FareView> Search(FareView data)
        {
            return _fareViewMgr.Search(data);
        }
        #endregion

        #region ScheduleView

        //public ScheduleView Get(ScheduleView data)
        //{
        //    return _scheduleViewMgr.Get(data);
        //}

        public ICollection<ScheduleView> GetSchedules()
        {
            return _scheduleViewMgr.Gets();
        }

        public ICollection<ScheduleView> Search(ScheduleView data)
        {
            return _scheduleViewMgr.Search(data);
        }
        #endregion

        #region CustomerView

        //public ScheduleView Get(ScheduleView data)
        //{
        //    return _scheduleViewMgr.Get(data);
        //}

        public ICollection<CustomerView> GetCustomers()
        {
            return _customerViewMgr.Gets();
        }

        public ICollection<CustomerView> Search(CustomerView data)
        {
            return _customerViewMgr.Search(data);
        }
        #endregion

        #region TicketView

        //public TicketView Get(TicketView data)
        //{
        //    return _ticketViewMgr.Get(data);
        //}

        public ICollection<TicketView> GetTickets()
        {
            return _ticketViewMgr.Gets();
        }

        public ICollection<TicketView> Search(TicketView data)
        {
            return _ticketViewMgr.Search(data);
        }
        #endregion
    }
}
