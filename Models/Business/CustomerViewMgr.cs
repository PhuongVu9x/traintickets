﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.modelViews;

namespace DemoAspNetCore.Models.Business
{
    public class CustomerViewMgr : IModelView<CustomerView>
    {
        public CustomerView Get(CustomerView data)
        {
            throw new NotImplementedException();
        }

        public ICollection<CustomerView> Gets()
        {
            return ModelMaker.Instance.GetCustomers().Select(item => new CustomerView(item)).ToList();
        }

        public ICollection<CustomerView> Search(CustomerView data)
        {
            return Gets().Where(item => item.NameCustomer.ToLower().Contains(data.NameCustomer.ToLower())
                                     || item.EmailCustomer.ToLower().Contains(data.EmailCustomer.ToLower())
                                     || item.ContactCustomer.ToLower().Contains(data.ContactCustomer.ToLower())
                                     || item.DobCustomer.ToLower().Contains(data.DobCustomer.ToLower())).ToList();
        }
    }
}
