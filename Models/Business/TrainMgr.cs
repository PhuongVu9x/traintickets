﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.Business
{
    public class TrainMgr : IModelCrud<Train>, IModelView<Train>
    {
        public string ChangeStatus(Train data)
        {
            try
            {
                using var db = new TrainTicketContext();
                var train = db.Trains.FirstOrDefault(item => item.IdTrain == data.IdTrain);
                if (train is null) return AppMgr.Fail;

                train.StatusTrain = data.StatusTrain;
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public Train Get(Train data)
        {
            using var db = new TrainTicketContext();
            return db.Trains.FirstOrDefault(item => item.IdTrain == data.IdTrain
                                                 || item.CodeTrain.Equals(data.CodeTrain) 
                                                 || item.NameTrain.Equals(data.NameTrain));
        }

        public ICollection<Train> Gets()
        {
            using var db = new TrainTicketContext();
            return db.Trains.OrderByDescending(item => item.IdTrain).ToList();
        }

        public string Insert(Train data)
        {
            try
            {
                using var db = new TrainTicketContext();
                data.StatusTrain = true;
                db.Trains.Add(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public bool IsExists(Train data)
        {
            throw new NotImplementedException();
        }

        public ICollection<Train> Search(Train data)
        {
            throw new NotImplementedException();
        }

        public string Update(Train data)
        {
            try
            {
                using var db = new TrainTicketContext();
                db.Trains.Update(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }
    }
}
