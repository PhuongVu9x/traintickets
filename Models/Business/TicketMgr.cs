﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.Business
{
    public class TicketMgr : IModelCrud<Ticket>, IModelView<Ticket>
    {
        public string ChangeStatus(Ticket data)
        {
            try
            {
                using var db = new TrainTicketContext();
                var ticket = db.Tickets.FirstOrDefault(item => item.IdTicket == data.IdTicket);
                if (ticket is null) return AppMgr.Fail;

                ticket.StatusTicket = data.StatusTicket;
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public Ticket Get(Ticket data)
        {
            using var db = new TrainTicketContext();
            return db.Tickets.FirstOrDefault(item => item.IdTicket == data.IdTicket);
        }

        public ICollection<Ticket> Gets()
        {
            using var db = new TrainTicketContext();
            return db.Tickets.OrderByDescending(item => item.IdTicket).ToList();
        }

        public string Insert(Ticket data)
        {
            try
            {
                using var db = new TrainTicketContext();
                db.Tickets.Add(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public bool IsExists(Ticket data)
        {
            using var db = new TrainTicketContext();
            return db.Tickets.Any(item => item.IdSchedule == data.IdSchedule && item.IdSeat.Equals(data.IdSeat));
        }

        public ICollection<Ticket> Search(Ticket data)
        {
            throw new NotImplementedException();
        }

        public string Update(Ticket data)
        {
            try
            {
                using var db = new TrainTicketContext();
                db.Tickets.Update(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }
    }
}
