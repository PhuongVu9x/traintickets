﻿namespace DemoAspNetCore.Models.Business
{
    public class StatusMgr
    {
        const string _inactive = "Inactive";
        const string _active = "Active";
        const string _repair = "Repair";
        const string _upgrade = "Upgrade";
        const string _paid = "Paid";
        const string _unpaid= "Unpaid";

        public static bool GetStatus(string data)
        {
            return data == _active ? true : false;
        }

        public static string GetViewStatus(bool data)
        {
            return data ? _inactive : _active;
        }

        public static bool TicketStatus(string data)
        {
            return data == _paid ? true : false;
        }

        public static string TicketViewStatus(bool data)
        {
            return data ? _paid : _unpaid;
        }

        public static List<string> AllStationStatus()
        {
            return new List<string>
            {
                StationStatus.Inactive.ToString(),
                StationStatus.Active.ToString(),
                StationStatus.Repair.ToString(),
                StationStatus.Upgrade.ToString(),
            };
        }

        public static int GetStationStatus(string data)
        {
            return data switch
            {
                _inactive => 0,
                _active => 1,
                _repair => 2,
                _upgrade => 3,
                _ => 0,
            };
        }

        public static string GetStationViewStatus(int data)
        {
            return data switch
            {
                0 => StationStatus.Inactive.ToString(),
                1 => StationStatus.Active.ToString(),
                2 => StationStatus.Repair.ToString(),
                3 => StationStatus.Upgrade.ToString(),
                _ => StationStatus.Inactive.ToString(),
            };
        }
    }

    public enum StationStatus
    {
        Inactive = 0,
        Active = 1,
        Repair = 2,
        Upgrade = 3,
    }
}
