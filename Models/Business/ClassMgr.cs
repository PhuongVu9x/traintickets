﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.Business
{
    public class ClassMgr : IModelCrud<Class>, IModelView<Class>
    {
        public string ChangeStatus(Class data)
        {
            try
            {
                using var db = new TrainTicketContext();
                var tmpClass = db.Classes.FirstOrDefault(item => item.IdClass == data.IdClass);
                if (tmpClass is null) return AppMgr.Fail;

                tmpClass.StatusClass = data.StatusClass;
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public Class Get(Class data)
        {
            using var db = new TrainTicketContext();
            return db.Classes.FirstOrDefault(item => item.IdClass == data.IdClass || item.NameClass.Equals(data.NameClass));
        }

        public ICollection<Class> Gets()
        {
            using var db = new TrainTicketContext();
            return db.Classes.OrderByDescending(item => item.IdClass).ToList();
        }

        public string Insert(Class data)
        {
            try
            {
                using var db = new TrainTicketContext();
                data.StatusClass = true;
                db.Classes.Add(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public bool IsExists(Class data)
        {
            throw new NotImplementedException();
        }

        public ICollection<Class> Search(Class data)
        {
            throw new NotImplementedException();
        }

        public string Update(Class data)
        {
            try
            {
                using var db = new TrainTicketContext();
                db.Classes.Update(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }
    }
}
