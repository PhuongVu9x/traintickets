﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.Business
{
    public class StationMgr : IModelCrud<Station>, IModelView<Station>
    {
        public string ChangeStatus(Station data)
        {
            try
            {
                using var db = new TrainTicketContext();
                var station = db.Stations.FirstOrDefault(item => item.IdStation == data.IdStation);
                if (station is null) return AppMgr.Fail;

                station.StatusStation = data.StatusStation;
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public Station Get(Station data)
        {
            using var db = new TrainTicketContext();
            return db.Stations.FirstOrDefault(item => item.IdStation == data.IdStation 
                                                   || item.CodeStation.Equals(data.CodeStation) 
                                                   || item.NameStation.Equals(data.NameStation));
        }

        public ICollection<Station> Gets()
        {
            using var db = new TrainTicketContext();
            return db.Stations.OrderByDescending(item => item.IdStation).ToList();
        }

        public string Insert(Station data)
        {
            try
            {
                using var db = new TrainTicketContext();
                data.StatusStation = true;
                db.Stations.Add(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public bool IsExists(Station data)
        {
            throw new NotImplementedException();
        }

        public ICollection<Station> Search(Station data)
        {
            throw new NotImplementedException();
        }

        public string Update(Station data)
        {
            try
            {
                using var db = new TrainTicketContext();
                db.Stations.Update(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }
    }
}
