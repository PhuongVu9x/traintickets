﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.modelViews;

namespace DemoAspNetCore.Models.Business
{
    public class AdminViewMgr : IModelView<AdminView>
    {
        public AdminView Get(AdminView data)
        {
            throw new NotImplementedException();
        }

        public ICollection<AdminView> Gets()
        {
            return ModelMaker.Instance.GetAdmins().Select(item => new AdminView(item)).ToList();
        }

        public ICollection<AdminView> Search(AdminView data)
        {
            return Gets().Where(item => item.NameAdmin.ToLower().Contains(data.NameAdmin.ToLower()) 
                                     || item.EmailAdmin.ToLower().Contains(data.EmailAdmin.ToLower()) 
                                     || item.ContactAdmin.ToLower().Contains(data.ContactAdmin.ToLower())).ToList();
        }
    }
}
