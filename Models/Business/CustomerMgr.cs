﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.Business
{
    public class CustomerMgr : IModelCrud<Customer>, IModelView<Customer>
    {
        public string ChangeStatus(Customer data)
        {
            try
            {
                using var db = new TrainTicketContext();
                var customer = db.Customers.FirstOrDefault(item => item.IdCustomer == data.IdCustomer);
                if (customer is null) return AppMgr.Fail;

                customer.StatusCustomer = data.StatusCustomer;
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public Customer Get(Customer data)
        {
            using var db = new TrainTicketContext();
            return db.Customers.FirstOrDefault(item => item.IdCustomer == data.IdCustomer
                                                 || item.GoogleId.Equals(data.GoogleId) 
                                                 || item.FacebookId.Equals(data.FacebookId)
                                                 || item.EmailCustomer.Equals(data.EmailCustomer) && item.TokenCustomer.Equals(data.DobCustomer)
                                                 || item.EmailCustomer.Equals(data.EmailCustomer) && item.PasswordCustomer.Equals(data.PasswordCustomer));
        }

        public ICollection<Customer> Gets()
        {
            using var db = new TrainTicketContext();
            return db.Customers.OrderByDescending(item => item.IdCustomer).ToList();
        }

        public string Insert(Customer data)
        {
            try
            {
                using var db = new TrainTicketContext();
                data.AvatarCustomer = "defaultAvatar.jpg";
                data.StatusCustomer = false;
                db.Customers.Add(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public bool IsExists(Customer data)
        {
            using var db = new TrainTicketContext();
            return db.Customers.Any(item => item.EmailCustomer.Equals(data.EmailCustomer) 
                                         || item.GoogleId.Equals(data.GoogleId) 
                                         || item.FacebookId.Equals(data.FacebookId));
        }

        public ICollection<Customer> Search(Customer data)
        {
            throw new NotImplementedException();
        }

        public string Update(Customer data)
        {
            try
            {
                using var db = new TrainTicketContext();
                db.Customers.Update(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }
    }
}
