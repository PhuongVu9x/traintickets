﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.Business
{
    public class FareMgr : IModelCrud<Fare>, IModelView<Fare>
    {
        public string ChangeStatus(Fare data)
        {
            try
            {
                using var db = new TrainTicketContext();
                var fare = db.Fares.FirstOrDefault(item => item.IdFare == data.IdFare);
                if (fare is null) return AppMgr.Fail;

                fare.StatusFare = data.StatusFare;
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public Fare Get(Fare data)
        {
            using var db = new TrainTicketContext();
            return db.Fares.FirstOrDefault(item => item.IdFare == data.IdFare);
        }

        public ICollection<Fare> Gets()
        {
            using var db = new TrainTicketContext();
            return db.Fares.OrderByDescending(item => item.IdFare).ToList();
        }

        public string Insert(Fare data)
        {
            try
            {
                using var db = new TrainTicketContext();
                data.StatusFare = true;
                db.Fares.Add(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public bool IsExists(Fare data)
        {
            throw new NotImplementedException();
        }

        public ICollection<Fare> Search(Fare data)
        {
            throw new NotImplementedException();
        }

        public string Update(Fare data)
        {
            try
            {
                using var db = new TrainTicketContext();
                db.Fares.Update(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }
    }
}
