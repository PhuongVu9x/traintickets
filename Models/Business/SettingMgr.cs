﻿using Newtonsoft.Json;

namespace DemoAspNetCore.Models.Business
{
    public class SettingMgr
    {
        public static void Debug(object data) =>
            Console.WriteLine(JsonConvert.SerializeObject(data));
    }
}
