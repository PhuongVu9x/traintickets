﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.Business
{
    public class AdminMgr : IModelCrud<Admin>, IModelView<Admin>
    {
        public string ChangeStatus(Admin data)
        {
            try
            {
                using var db = new TrainTicketContext();
                var admin = db.Admins.FirstOrDefault(item => item.IdAdmin == data.IdAdmin);
                if (admin is null) return AppMgr.Fail;

                admin.StatusAdmin = data.StatusAdmin;
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public Admin Get(Admin data)
        {
            using var db = new TrainTicketContext();
            return db.Admins.FirstOrDefault(item => item.IdAdmin == data.IdAdmin 
                                                 || item.EmailAdmin.Equals(data.EmailAdmin) && item.PasswordAdmin.Equals(data.PasswordAdmin));
        }

        public ICollection<Admin> Gets()
        {
            using var db = new TrainTicketContext();
            return db.Admins.OrderByDescending(item => item.IdAdmin).ToList();
        }

        public string Insert(Admin data)
        {
            try
            {
                using var db = new TrainTicketContext();
                data.AvatarAdmin = "defaultAvatar.jpg";
                data.StatusAdmin = true;
                db.Admins.Add(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public bool IsExists(Admin data)
        {
            throw new NotImplementedException();
        }

        public ICollection<Admin> Search(Admin data)
        {
            throw new NotImplementedException();
        }

        public string Update(Admin data)
        {
            try
            {
                using var db = new TrainTicketContext();
                db.Admins.Update(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }
    }
}
