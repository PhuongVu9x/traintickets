﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.modelViews;

namespace DemoAspNetCore.Models.Business
{
    public class FareViewMgr : IModelView<FareView>
    {
        public FareView Get(FareView data)
        {
            return Gets().FirstOrDefault(item => item.IdFare == data.IdFare);
        }

        public ICollection<FareView> Gets()
        {
            return ModelMaker.Instance.GetFares().Select(item => new FareView(item)).ToList();
        }

        public ICollection<FareView> Search(FareView data)
        {
            return Gets().Where(item => item.FromFareName.ToLower().Contains(data.FromFareName.ToLower())
                                     || item.ToFareName.ToLower().Contains(data.ToFareName.ToLower())
                                     || item.PriceFare == data.PriceFare).ToList();
        }
    }
}
