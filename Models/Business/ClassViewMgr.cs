﻿using DemoAspNetCore.Models.modelViews;
using DemoAspNetCore.Models.designPatterns;

namespace DemoAspNetCore.Models.Business
{
    public class ClassViewMgr : IModelView<ClassView>
    {
        public ClassView Get(ClassView data)
        {
            throw new NotImplementedException();
        }

        public ICollection<ClassView> Gets()
        {
            return ModelMaker.Instance.GetClasses().Select(item => new ClassView(item)).ToList();
        }

        public ICollection<ClassView> Search(ClassView data)
        {
            return Gets().Where(item => item.NameClass.ToLower().Contains(data.NameClass.ToLower())
                                     || item.RateClass == data.RateClass).ToList();
        }
    }
}
