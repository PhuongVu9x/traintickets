﻿namespace DemoAspNetCore.Models.Business
{
    public class EventMgr 
    {
        public class Admin
        {
            public static string RESULT_CRUD_ADMIN = "Result CRUD Admin";
            public static string SEARCH_ADMIN = "Search Admin";
        }

        public class Station
        {
            public static string RESULT_CRUD_STATION = "Result CRUD Station";
            public static string SEARCH_STATION = "Search Station";
        }

        public class Train
        {
            public static string RESULT_CRUD_TRAIN = "Result CRUD Train";
            public static string SEARCH_TRAIN = "Search Train";
        }

        public class Class
        {
            public static string RESULT_CRUD_CLASS = "Result CRUD Class";
            public static string SEARCH_CLASS = "Search Class";
        }
        
        public class Fare
        {
            public static string RESULT_CRUD_FARE = "Result CRUD Fare";
            public static string SEARCH_FARE = "Search Fare";
        }

        public class Schedule 
        {
            public static string RESULT_CRUD_SCHEDULE = "Result CRUD Schedule";
            public static string SEARCH_SCHEDULE = "Search Schedule";
        }

        public class Customer 
        {
            public static string LOGIN_CUSTOMER = "Login Customer";
            public static string LOGIN_RESULT_CUSTOMER = "Login Result Customer";
            public static string CREATE_ACCOUNT_CUSTOMER = "Create Account Customer";
            public static string RENEW_SESSION_CUSTOMER = "Renew Session Customer";
            public static string RESULT_CRUD_CUSTOMER = "Result CRUD Customer";
            public static string SEARCH_CUSTOMER = "Search Customer";
        }

        public class Ticket 
        {
            public static string RESULT_CRUD_TICKET = "Result CRUD Ticket";
            public static string SEARCH_TICKET = "Search Ticket";
        }
    }
}
