﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.modelViews;

namespace DemoAspNetCore.Models.Business
{
    public class ScheduleViewMgr : IModelView<ScheduleView>
    {
        public ScheduleView Get(ScheduleView data)
        {
            throw new NotImplementedException();
        }

        public ICollection<ScheduleView> Gets()
        {
            return ModelMaker.Instance.GetSchedules().Select(item => new ScheduleView(item)).ToList();
        }

        public ICollection<ScheduleView> Search(ScheduleView data)
        {
            return Gets().Where(item => item.NameTrain.ToLower().Contains(data.NameTrain.ToLower())
                                     || item.FromFareName.ToLower().Contains(data.FromFareName.ToLower())
                                     || item.ToFareName.ToLower().Contains(data.ToFareName.ToLower())
                                     || item.DepartureSchedule.ToLower().Contains(data.DepartureSchedule.ToLower())
                                     || item.ArrivalSchedule.ToLower().Contains(data.ArrivalSchedule.ToLower())).ToList();
        }
    }
}
