﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.modelViews;

namespace DemoAspNetCore.Models.Business
{
    public class TrainViewMgr : IModelView<TrainView>
    {
        public TrainView Get(TrainView data)
        {
            throw new NotImplementedException();
        }

        public ICollection<TrainView> Gets()
        {
           return ModelMaker.Instance.GetTrains().Select(item => new TrainView(item)).ToList();
        }

        public ICollection<TrainView> Search(TrainView data)
        {
            return Gets().Where(item => item.NameTrain.ToLower().Contains(data.NameTrain.ToLower())
                                     || item.CodeTrain.ToLower().Contains(data.CodeTrain.ToLower())
                                     || item.Ac1Seat == data.Ac1Seat || item.Ac2Seat == data.Ac2Seat
                                     || item.Ac3Seat == data.Ac3Seat || item.FcSeat == data.FcSeat
                                     || item.ScSeat == data.ScSeat).ToList();
        }
    }
}
