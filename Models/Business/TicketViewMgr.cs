﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.modelViews;

namespace DemoAspNetCore.Models.Business
{
    public class TicketViewMgr : IModelView<TicketView>
    {
        public TicketView Get(TicketView data)
        {
            throw new NotImplementedException();
        }

        public ICollection<TicketView> Gets()
        {
            return ModelMaker.Instance.GetTickets().Select(item => new TicketView(item)).ToList();
        }

        public ICollection<TicketView> Search(TicketView data)
        {
            return Gets().Where(item => item.FromSchedule.ToLower().Contains(data.FromSchedule.ToLower())
                                     || item.ToSchedule.ToLower().Contains(data.ToSchedule.ToLower())
                                     || item.NameCustomer.ToLower().Contains(data.NameCustomer.ToLower())
                                     || item.NameClass.ToLower().Contains(data.NameClass.ToLower())
                                     || item.IdSeat.ToLower().Contains(data.IdSeat.ToLower())
                                     || item.PriceTicket == data.PriceTicket).ToList();
        }
    }
}
