﻿using Elfie.Serialization;
using Newtonsoft.Json;
using System;
using System.Security.Cryptography;
using System.Text;

namespace DemoAspNetCore.Models.Business
{
    public class AppMgr
    {
        public static string Successfull = "Successfull.";
        public static string Fail = "Fail.";
        public static string ServerError = "Something wrong in server.";
        public static string EmailError = "Send email fail.";

        public static string ToJson(object data)
        {
            return JsonConvert.SerializeObject(data);
        }

        public static T GetJson<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }

        public static string EncryptASCII(string input)
        {
            //MD5 md5Hash = MD5.Create();

            //byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            //StringBuilder sBuilder = new StringBuilder();
            //for (int i = 0; i < data.Length; i++)
            //{
            //    sBuilder.Append(data[i].ToString("x2"));
            //}
            //return sBuilder.ToString();
            byte[] data = ASCIIEncoding.ASCII.GetBytes(input);

            var test = Convert.ToBase64String(data);
            return test;
        }

        public static string DecryptASCII(string input)
        {
            //MD5 md5Hash = MD5.Create();

            //byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            //StringBuilder sBuilder = new StringBuilder();
            //for (int i = 0; i < data.Length; i++)
            //{
            //    sBuilder.Append(data[i].ToString("x2"));
            //}
            //return sBuilder.ToString();

            byte[] data = Convert.FromBase64String(input);

            var test = ASCIIEncoding.ASCII.GetString(data);
            return test;
        }

        public static string GeneratorToken(int length = 15)
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static String HmacSHA512(string key, String inputData)
        {
            var hash = new StringBuilder();
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);
            byte[] inputBytes = Encoding.UTF8.GetBytes(inputData);
            using (var hmac = new HMACSHA512(keyBytes))
            {
                byte[] hashValue = hmac.ComputeHash(inputBytes);
                foreach (var theByte in hashValue)
                {
                    hash.Append(theByte.ToString("x2"));
                }
            }

            return hash.ToString();
        }
    }
}
