﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.Business
{
    public class ScheduleMgr : IModelCrud<Schedule>, IModelView<Schedule>
    {
        public string ChangeStatus(Schedule data)
        {
            try
            {
                using var db = new TrainTicketContext();
                var schedule = db.Schedules.FirstOrDefault(item => item.IdSchedule == data.IdSchedule);
                if (schedule is null) return AppMgr.Fail;

                schedule.StatusSchedule = data.StatusSchedule;
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public Schedule Get(Schedule data)
        {
            using var db = new TrainTicketContext();
            return db.Schedules.FirstOrDefault(item => item.IdSchedule == data.IdSchedule);
        }

        public ICollection<Schedule> Gets()
        {
            using var db = new TrainTicketContext();
            return db.Schedules.OrderByDescending(item => item.IdSchedule).ToList();
        }

        public string Insert(Schedule data)
        {
            try
            {
                using var db = new TrainTicketContext();
                data.StatusSchedule = true;
                db.Schedules.Add(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }

        public bool IsExists(Schedule data)
        {
            throw new NotImplementedException();
        }

        public ICollection<Schedule> Search(Schedule data)
        {
            throw new NotImplementedException();
        }

        public string Update(Schedule data)
        {
            try
            {
                using var db = new TrainTicketContext();
                db.Schedules.Update(data);
                db.SaveChanges();
                return AppMgr.Successfull;
            }
            catch (Exception)
            {
                return AppMgr.Fail;
            }
        }
    }
}
