﻿
using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.modelViews;

namespace DemoAspNetCore.Models.Business
{
    public class StationViewMgr : IModelView<StationView>
    {
        public StationView Get(StationView data)
        {
            throw new NotImplementedException();
        }

        public ICollection<StationView> Gets()
        {
            return ModelMaker.Instance.GetStations().Select(item => new StationView(item)).ToList();
        }

        public ICollection<StationView> Search(StationView data)
        {
            return Gets().Where(item => item.CodeStation.ToLower().Contains(data.CodeStation.ToLower())
                                     || item.NameStation.ToLower().Contains(data.NameStation.ToLower())).ToList();
        }
    }
}
