﻿using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.modelViews
{
    public sealed class ClassView
    {
        public int IdClass { get; set; }
        public string NameClass { get; set; }
        public float RateClass { get; set; }
        public bool StatusClass { get; set; }

        public ClassView()
        {

        }

        public ClassView(string text)
        {
            NameClass = text;
            _ = float.TryParse(text, out float rate);
            RateClass = rate;
        }

        public ClassView(Class data)
        {
            IdClass = data.IdClass;
            NameClass = data.NameClass;
            RateClass = data.RateClass;
            StatusClass = data.StatusClass;
        }
    }
}
