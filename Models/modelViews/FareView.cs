﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;
using System.ComponentModel.DataAnnotations;

namespace DemoAspNetCore.Models.modelViews
{
    public sealed class FareView
    {
        public int IdFare { get; set; }
        public int FromFare { get; set; }
        public string FromFareName { get; set; }
        public int ToFare { get; set; }
        public string ToFareName { get; set; }
        public decimal PriceFare { get; set; }
        public bool StatusFare { get; set; }

        public FareView() { }

        public FareView(int id) 
        {
            IdFare = id;
        }

        public FareView(string text)
        {
            FromFareName = text;
            ToFareName = text;
            _ = decimal.TryParse(text,out decimal price);
            PriceFare = price;
        }

        public FareView(Fare fare)
        {
            IdFare = fare.IdFare;
            FromFare = fare.FromFare;
            FromFareName = ModelMaker.Instance.Get(new Station(FromFare)).NameStation;
            ToFare = fare.ToFare;
            ToFareName = ModelMaker.Instance.Get(new Station(ToFare)).NameStation;
            PriceFare = fare.PriceFare;
            StatusFare = fare.StatusFare;
        }
    }
}
