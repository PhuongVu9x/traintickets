﻿using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.modelViews
{
    public sealed class CustomerView
    {
        public int IdCustomer { get; set; }
        public string NameCustomer { get; set; }
        public string EmailCustomer { get; set; }
        public string PasswordCustomer { get; set; }
        public string ContactCustomer { get; set; }
        public string DobCustomer { get; set; }
        public string AvatarCustomer { get; set; }
        public string GoogleId{ get; set; }
        public string FacebookId { get; set; }
        public int LoginStatus { get; set; }
        public string TokenCustomer { get; set; }
        public bool StatusCustomer { get; set; }

        public CustomerView()
        {

        }

        public CustomerView(string text)
        {
            NameCustomer = text;
            EmailCustomer = text;
            ContactCustomer = text;
            DobCustomer = text;
        }

        public CustomerView(Customer customer)
        {
            IdCustomer = customer.IdCustomer;
            NameCustomer = customer.NameCustomer;
            EmailCustomer = customer.EmailCustomer;
            DobCustomer = customer.DobCustomer;
            PasswordCustomer = customer.PasswordCustomer;
            ContactCustomer = customer.ContactCustomer;
            AvatarCustomer = customer.AvatarCustomer;
            TokenCustomer = customer.TokenCustomer;
            GoogleId = customer.GoogleId; 
            FacebookId = customer.FacebookId;
            LoginStatus = customer.LoginStatus;
            StatusCustomer = customer.StatusCustomer;
        }
    }
}
