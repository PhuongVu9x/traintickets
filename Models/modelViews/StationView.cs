﻿using DemoAspNetCore.Models.entities;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DemoAspNetCore.Models.modelViews
{
    public sealed class StationView
    {
        public int IdStation { get; set; }
        public string CodeStation { get; set; }
        public string NameStation { get; set; }
        public bool StatusStation { get; set; }

        public ICollection<Fare> Fares { get; set; }

        public StationView() { }

        public StationView(string text)
        {
            CodeStation = text;
            NameStation = text;
        }

        public StationView(Station station)
        {
            IdStation = station.IdStation;
            CodeStation = station.CodeStation;
            NameStation = station.NameStation;
            StatusStation = station.StatusStation;
        }
    }
}
