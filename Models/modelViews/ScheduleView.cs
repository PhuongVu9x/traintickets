﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.modelViews
{
    public sealed class ScheduleView
    {
        public int IdSchedule { get; set; }
        public int IdTrain { get; set; }
        public string NameTrain { get; set; }
        public int IdFare { get; set; }
        public string FromFareName { get; set; }
        public int FromFare { get; set; }
        public string ToFareName { get; set; }
        public int ToFare { get; set; }
        public string DepartureSchedule { get; set; }
        public string ArrivalSchedule { get; set; }
        public bool StatusSchedule { get; set; }

        public ScheduleView()
        {

        }

        public ScheduleView(string text)
        {
            NameTrain = text;
            FromFareName = text;
            ToFareName = text;
            DepartureSchedule = text;
            ArrivalSchedule = text;
        }

        public ScheduleView(Schedule schedule)
        {
            IdSchedule = schedule.IdSchedule;
            IdTrain = schedule.IdTrain;
            IdFare = schedule.IdFare;

            //NameTrain = schedule.Train.NameTrain;
            NameTrain = ModelMaker.Instance.Get(new Train(IdTrain)).NameTrain;

            //var fare = schedule.Fare;
            //FromFareName = ModelMaker.Instance.Get(new Station(fare.FromFare)).NameStation;
            //ToFareName = ModelMaker.Instance.Get(new Station(fare.ToFare)).NameStation;

            var fare = ModelMaker.Instance.Get(new Fare(IdFare));
            FromFareName = ModelMaker.Instance.Get(new Station(fare.FromFare)).NameStation;
            FromFare = ModelMaker.Instance.Get(new Station(fare.FromFare)).IdStation;
            ToFareName = ModelMaker.Instance.Get(new Station(fare.ToFare)).NameStation;
            ToFare = ModelMaker.Instance.Get(new Station(fare.ToFare)).IdStation;

            DepartureSchedule = schedule.DepartureSchedule;
            ArrivalSchedule = schedule.ArrivalSchedule;
            StatusSchedule = schedule.StatusSchedule;
        }
    }
}
