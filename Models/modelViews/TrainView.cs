﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using DemoAspNetCore.Models.entities;

namespace DemoAspNetCore.Models.modelViews
{
    public sealed class TrainView
    {
        public int IdTrain { get; set; }
        public string CodeTrain { get; set; }
        public string NameTrain { get; set; }
        public int Ac1Seat { get; set; }
        public int Ac2Seat { get; set; }
        public int Ac3Seat { get; set; }
        public int FcSeat { get; set; }
        public int ScSeat { get; set; }
        public bool StatusTrain { get; set; }

        public TrainView() { }

        public TrainView(string text)
        {
            CodeTrain = text;
            NameTrain = text;
            _ = Int32.TryParse(text, out int quantity);
            Ac1Seat = quantity;
            Ac2Seat = quantity;
            Ac3Seat = quantity;
            FcSeat = quantity;
            ScSeat = quantity;
        }

        public TrainView(Train train)
        {
            IdTrain = train.IdTrain;
            CodeTrain = train.CodeTrain;
            NameTrain = train.NameTrain;
            Ac1Seat = train.Ac1Seat;
            Ac2Seat = train.Ac2Seat;
            Ac3Seat = train.Ac3Seat;
            FcSeat = train.FcSeat;
            ScSeat = train.ScSeat;
            StatusTrain = train.StatusTrain;
        }
    }
}
