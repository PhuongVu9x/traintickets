﻿using DemoAspNetCore.Models.entities;
using System.ComponentModel.DataAnnotations;

namespace DemoAspNetCore.Models.modelViews
{
    public sealed class AdminView
    {
        public int IdAdmin { get; set; }
        public string NameAdmin { get; set; }
        public string EmailAdmin { get; set; }
        public string PasswordAdmin { get; set; }
        public string ContactAdmin { get; set; }
        public string AvatarAdmin { get; set; }
        public bool StatusAdmin { get; set; }
        public AdminView() { }

        public AdminView(string text)
        {
            NameAdmin = text;
            EmailAdmin = text;
            ContactAdmin = text;
        }

        public AdminView(Admin admin)
        {
            IdAdmin = admin.IdAdmin;
            NameAdmin = admin.NameAdmin;
            EmailAdmin = admin.EmailAdmin;
            ContactAdmin = admin.ContactAdmin;
            PasswordAdmin = admin.PasswordAdmin;
            AvatarAdmin = admin.AvatarAdmin;
            StatusAdmin = admin.StatusAdmin;
        }
    }
}
