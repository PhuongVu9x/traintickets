﻿using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.Business;

namespace DemoAspNetCore.Models.modelViews
{
    public sealed class TicketView
    {
        public int IdTicket { get; set; }
        public int IdSchedule { get; set; }
        public string FromSchedule { get; set; }
        public string ToSchedule { get; set; }
        public int IdCustomer { get; set; }
        public string Departure { get; set; } 
        public string Arrival { get; set; } 
        public string NameCustomer { get; set; } 
        public int IdClass { get; set; }
        public string NameClass { get; set; }
        public string IdSeat { get; set; }
        public decimal PriceTicket { get; set; }
        public string StatusTicket { get; set; }

        public TicketView()
        {

        }

        public TicketView(string text)
        {
            FromSchedule = text;
            ToSchedule = text;
            NameCustomer = text;
            NameClass = text;
            _ = decimal.TryParse(text, out decimal price);
            PriceTicket = price;
            IdSeat = text;
            StatusTicket = text;
        }

        public TicketView(entities.Ticket ticket)
        {
            IdTicket = ticket.IdTicket;
            IdSchedule = ticket.IdSchedule;
            //FromSchedule = ticket.Schedule.Fare.FromStation.NameStation;
            //ToSchedule = ticket.Schedule.Fare.ToStation.NameStation;
            //FromSchedule = ModelMaker.Instance.Get(new entities.Station(ticket.Schedule.Fare.FromFare)).NameStation;
            //ToSchedule = ModelMaker.Instance.Get(new entities.Station(ticket.Schedule.Fare.ToFare)).NameStation;

            var schedule = ModelMaker.Instance.Get(new entities.Schedule(IdSchedule));
            var fareView = ModelViewMaker.Instance.Get(new FareView(schedule.IdFare));
            FromSchedule = fareView.FromFareName;
            ToSchedule = fareView.ToFareName;

            Departure = schedule.DepartureSchedule;
            Arrival = schedule.ArrivalSchedule;

            IdCustomer = ticket.IdCustomer;
            //NameCustomer = ticket.Customer.NameCustomer;
            NameCustomer = ModelMaker.Instance.Get(new entities.Customer(IdCustomer)).NameCustomer;

            IdClass = ticket.IdClass;
            //NameClass = ticket.Class.NameClass;
            NameClass = ModelMaker.Instance.Get(new entities.Class(IdClass)).NameClass;

            IdSeat = ticket.IdSeat;
            PriceTicket = ticket.PriceTicket;
            StatusTicket = StatusMgr.TicketViewStatus(ticket.StatusTicket);
        }
    }
}
