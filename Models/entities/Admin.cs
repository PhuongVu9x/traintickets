﻿using DemoAspNetCore.Models.Business;
using DemoAspNetCore.Models.modelViews;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoAspNetCore.Models.entities
{
    [Table("Admin")]
    public class Admin
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdAdmin", TypeName = "int")]
        public int IdAdmin { get; set; }

        [Column("NameAdmin", TypeName = "nvarchar")]
        [StringLength(100)]
        public string NameAdmin { get; set; }

        [Column("EmailAdmin", TypeName = "varchar")]
        [StringLength(200)]
        public string EmailAdmin { get; set; }

        [Column("PasswordAdmin", TypeName = "varchar")]
        [StringLength(500)]
        public string PasswordAdmin { get; set; }

        [Column("ContactAdmin", TypeName = "varchar")]
        [StringLength(15)]
        public string ContactAdmin { get; set; }

        [Column("AvatarAdmin", TypeName = "varchar")]
        [StringLength(500)]
        public string AvatarAdmin { get; set; }

        [Column("StatusAdmin", TypeName = "bit")]
        public bool StatusAdmin { get; set; }

        public Admin() { }
        public Admin(int id)
        { 
            IdAdmin = id;
        }

        public Admin(AdminView adminView) 
        { 
            IdAdmin = adminView.IdAdmin;
            NameAdmin = adminView.NameAdmin;
            EmailAdmin = adminView.EmailAdmin;
            ContactAdmin = adminView.ContactAdmin;
            PasswordAdmin = adminView.PasswordAdmin;
            AvatarAdmin = adminView.AvatarAdmin;
            StatusAdmin = adminView.StatusAdmin;
        }
    }
}
