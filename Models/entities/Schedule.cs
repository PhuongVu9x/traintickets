﻿using DemoAspNetCore.Models.modelViews;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoAspNetCore.Models.entities
{
    [Table("Schedule")]
    public class Schedule
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdSchedule", TypeName = "int")]
        public int IdSchedule { get; set; }

        [Column("IdTrain", TypeName = "int")]
        public int IdTrain { get; set; }
        [ForeignKey("IdTrain")]
        public Train Train { get; set; }

        [Column("IdFare", TypeName = "int")]
        public int IdFare { get; set; }
        [ForeignKey("IdFare")]
        public Fare Fare { get; set; }

        [Column("DepartureSchedule", TypeName = "varchar")]
        [StringLength(50)]
        public string DepartureSchedule { get; set; }

        [Column("ArrivalSchedule", TypeName = "varchar")]
        [StringLength(50)]
        public string ArrivalSchedule { get; set; }

        [Column("StatusSchedule", TypeName = "bit")]
        public bool StatusSchedule { get; set; }

        public ICollection<Ticket> Tickets { get; set; }

        public Schedule() { }

        public Schedule(int id)
        {
            IdSchedule = id;
        }

        public Schedule(ScheduleView scheduleView)
        {
            IdSchedule = scheduleView.IdSchedule;
            IdTrain = scheduleView.IdTrain;
            IdFare = scheduleView.IdFare;
            DepartureSchedule = scheduleView.DepartureSchedule;
            ArrivalSchedule = scheduleView.ArrivalSchedule;
            StatusSchedule = scheduleView.StatusSchedule;
        }
    }
}
