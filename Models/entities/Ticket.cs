﻿using DemoAspNetCore.Models.Business;
using DemoAspNetCore.Models.modelViews;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoAspNetCore.Models.entities
{
    [Table("Ticket")]
    public class Ticket
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdTicket", TypeName = "int")]
        public int IdTicket { get; set; }

        [Column("IdSchedule", TypeName = "int")]
        public int IdSchedule { get; set; }
        [ForeignKey("IdSchedule")]
        public Schedule Schedule { get; set; }

        [Column("IdCustomer", TypeName = "int")]
        public int IdCustomer { get; set; }
        [ForeignKey("IdCustomer")]
        public Customer Customer { get; set; }

        [Column("IdClass", TypeName = "int")]
        public int IdClass { get; set; }
        [ForeignKey("IdClass")]
        public Class Class { get; set; }

        [Column("IdSeat", TypeName = "varchar")]
        [StringLength(5)]
        public string IdSeat { get; set; }

        [Column("PriceTicket", TypeName = "decimal")]
        public decimal PriceTicket { get; set; }

        [Column("StatusTicket", TypeName = "bit")]
        public bool StatusTicket { get; set; }

        public Ticket() { }

        public Ticket(int id)
        {
            IdTicket = id;
        }

        public Ticket(TicketView ticketView)
        {
            IdTicket = ticketView.IdTicket;
            IdSchedule = ticketView.IdSchedule;
            IdCustomer = ticketView.IdCustomer;
            IdClass = ticketView.IdClass;
            IdSeat = ticketView.IdSeat;
            PriceTicket = ticketView.PriceTicket;
            StatusTicket = StatusMgr.TicketStatus(ticketView.StatusTicket);
        }
    }
}
