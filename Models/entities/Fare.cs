﻿using DemoAspNetCore.Models.modelViews;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoAspNetCore.Models.entities
{
    [Table("Fare")]
    public class Fare
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdFare", TypeName = "int")]
        public int IdFare { get; set; }

        [Column("FromFare", TypeName = "int")]
        public int FromFare { get; set; }

        [Column("ToFare", TypeName = "int")]
        public int ToFare { get; set; }

        [Column("PriceFare", TypeName = "decimal")]
        public decimal PriceFare { get; set; }

        [Column("StatusFare", TypeName = "bit")]
        public bool StatusFare { get; set; }

        [InverseProperty(nameof(Schedule.Fare))]
        public ICollection<Schedule> Schedules { get; set; }

        public Fare() { }

        public Fare(int id)
        {
            IdFare = id;
        }

        public Fare(FareView fareView)
        {
            IdFare = fareView.IdFare;
            FromFare = fareView.FromFare;
            ToFare = fareView.ToFare;
            PriceFare = fareView.PriceFare;
            StatusFare = fareView.StatusFare;
        }
    }
}
