﻿using DemoAspNetCore.Models.modelViews;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoAspNetCore.Models.entities
{
    [Table("Class")]
    public class Class
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdClass", TypeName = "int")]
        public int IdClass { get; set; }

        [Column("NameClass", TypeName = "nvarchar")]
        [StringLength(100)]
        public string NameClass { get; set; }

        [Column("RateClass", TypeName = "float")]
        public float RateClass { get; set; }

        [Column("StatusClass", TypeName = "bit")]
        public bool StatusClass { get; set; }

        public ICollection<Ticket> Tickets { get; set; }

        public Class()
        {

        }

        public Class(int id)
        {
            IdClass = id;
        }

        public Class(ClassView classView)
        {
            IdClass = classView.IdClass;
            NameClass = classView.NameClass;
            RateClass = classView.RateClass;
            StatusClass = classView.StatusClass;
        }
    }
}
