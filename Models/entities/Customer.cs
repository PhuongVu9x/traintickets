﻿using DemoAspNetCore.Models.modelViews;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoAspNetCore.Models.entities
{
    [Table("Customer")]
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdCustomer", TypeName = "int")]
        public int IdCustomer { get; set; }

        [Column("NameCustomer", TypeName = "nvarchar")]
        [StringLength(100)]
        public string NameCustomer { get; set; }

        [Column("EmailCustomer", TypeName = "varchar")]
        [StringLength(200)]
        public string EmailCustomer { get; set; }

        [Column("PasswordCustomer", TypeName = "varchar")]
        [StringLength(500)]
        public string PasswordCustomer { get; set; }

        [Column("ContactCustomer", TypeName = "varchar")]
        [StringLength(100)]
        public string ContactCustomer { get; set; }

        [Column("DobCustomer", TypeName = "varchar")]
        [StringLength(50)]
        public string DobCustomer { get; set; }

        [Column("AvatarCustomer", TypeName = "varchar")]
        [StringLength(500)]
        public string AvatarCustomer { get; set; }
        
        [Column("TokenCustomer", TypeName = "varchar")]
        [StringLength(1000)]
        public string TokenCustomer { get; set; }

        [Column("GoogleId", TypeName = "varchar")]
        [StringLength(50)]
        public string GoogleId { get; set; }

        [Column("FacebookId", TypeName = "varchar")]
        [StringLength(50)]
        public string FacebookId { get; set; }

        [Column("LoginStatus", TypeName = "tinyint")]
        public int LoginStatus { get; set; }

        [Column("StatusCustomer", TypeName = "bit")]
        public bool StatusCustomer { get; set; }

        public ICollection<Ticket> Tickets { get; set; }

        public Customer()
        {

        }

        public Customer(int id)
        {
            IdCustomer = id;
        }

        public Customer(string email)
        {
            EmailCustomer = email;
        }
        
        public Customer(string email, string token)
        {
            EmailCustomer = email;
            TokenCustomer = token;
        }

        public Customer(CustomerView customerView)
        {
            IdCustomer = customerView.IdCustomer;
            NameCustomer = customerView.NameCustomer;
            EmailCustomer = customerView.EmailCustomer;
            PasswordCustomer = customerView.PasswordCustomer;
            ContactCustomer = customerView.ContactCustomer;
            DobCustomer = customerView.DobCustomer;
            AvatarCustomer = customerView.AvatarCustomer;
            TokenCustomer = customerView.TokenCustomer;
            GoogleId = customerView.GoogleId;
            FacebookId = customerView.FacebookId;
            LoginStatus = customerView.LoginStatus;
            StatusCustomer = customerView.StatusCustomer;
        }
    }
}
