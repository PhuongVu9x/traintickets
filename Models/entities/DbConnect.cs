﻿using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using PemUtils;
using System.Security.Cryptography;
using System.Text;

namespace DemoAspNetCore.Models.entities
{
    public class DbConnect
    {
        private static readonly Lazy<DbConnect> lazy = new(() => new DbConnect());
        public static DbConnect Instance => lazy.Value;
        //private static string format = "Data Source={0};Initial Catelog={1} ;Persist Security Info=True;User ID={2};Password={3}";
        //private static string server = "TrainTicket.mssql.somee.com";
        //private static string db = "TrainTicket";
        //private static string user = "newsdatabase_SQLLogin_1";
        //private static string password = "9dzsh7umb2";

        private static string format = "Server={0};Database={1};Trusted_Connection={2};MultipleActiveResultSets={3};TrustServerCertificate=True";
        private static string server = "DESKTOP-3SUF34M";
        private static string db = "TrainTicket";
        private static string user = "True";
        private static string password = "True";
        private static int keySize = 4096;

        public static void GenerateKeys()
        {
            using var rsa = GetRSA();
            using (var fs = File.Create(@"d:\private.pem"))
            {
                using var pem = new PemWriter(fs);
                pem.WritePrivateKey(rsa);
            }

            using (FileStream fs = File.Create(@"d:\public.pem"))
            {
                using var pem = new PemWriter(fs);
                pem.WritePublicKey(rsa);
            }
        }

        public static RSA GetRSA(string keyFileName = null)
        {
            var rsa = RSA.Create();

            if (keyFileName.IsNullOrEmpty())
                rsa.KeySize = keySize;
            else
                using (FileStream keyData = File.OpenRead(keyFileName))
                {
                    using var pem = new PemReader(keyData);
                    var rsaParams = pem.ReadRsaKey();
                    rsa.ImportParameters(rsaParams);
                }

            return rsa;
        }

        public static string Encrypt(string input = "", string keyFileName = @"d:\public.pem")
        {
            var connectString = String.Format(format, server, db, user, password);
            var text = string.Empty;
            using (var rsa = GetRSA(keyFileName))
            {
                var plainTextBytes = Encoding.Unicode.GetBytes(connectString);
                var cipherTextBytes = rsa.Encrypt(plainTextBytes, RSAEncryptionPadding.Pkcs1);
                text = Convert.ToBase64String(cipherTextBytes);
            }

            return text;
        }


        public static string Decrypt(string input = "", string keyFileName = @"d:\private.pem")
        {
            var connectString = Encrypt();
            var text = string.Empty;
            using (var rsa = GetRSA(keyFileName))
            {
                var cipherTextBytes = Convert.FromBase64String(connectString);
                var cipherTextByte = rsa.Decrypt(cipherTextBytes, RSAEncryptionPadding.Pkcs1);
                text = Encoding.Unicode.GetString(cipherTextByte);
            }

            return text;
        }

    }
}