﻿using DemoAspNetCore.Models.modelViews;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoAspNetCore.Models.entities
{
    [Table("Station")]
    public class Station
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdStation", TypeName = "int")]
        public int IdStation { get; set; }

        [Column("CodeStation", TypeName = "varchar")]
        [StringLength(10)]
        public string CodeStation { get; set; }

        [Column("NameStation", TypeName = "nvarchar")]
        [StringLength(200)]
        public string NameStation { get; set; }

        [Column("StatusStation", TypeName = "bit")]
        public bool StatusStation { get; set; }

        public Station() { }

        public Station(int id) 
        {
            IdStation = id;
        }
        public Station(StationView stationView)
        {
            IdStation = stationView.IdStation;
            CodeStation = stationView.CodeStation;
            NameStation = stationView.NameStation;
            StatusStation = stationView.StatusStation;
        }
    }
}
