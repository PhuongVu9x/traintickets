﻿using DemoAspNetCore.Models.modelViews;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoAspNetCore.Models.entities
{
    [Table("Train")]
    public class Train
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdTrain", TypeName = "int")]
        public int IdTrain { get; set; }

        [Column("CodeTrain", TypeName = "varchar")]
        [StringLength(200)]
        public string CodeTrain { get; set; }

        [Column("NameTrain", TypeName = "nvarchar")]
        [StringLength(200)]
        public string NameTrain { get; set; }

        [Column("Ac1Seat", TypeName = "int")]
        public int Ac1Seat { get; set; }

        [Column("Ac2Seat", TypeName = "int")]
        public int Ac2Seat { get; set; }

        [Column("Ac3Seat", TypeName = "int")]
        public int Ac3Seat { get; set; }

        [Column("FcSeat", TypeName = "int")]
        public int FcSeat { get; set; }

        [Column("ScSeat", TypeName = "int")]
        public int ScSeat { get; set; }

        [Column("StatusTrain", TypeName = "bit")]
        public bool StatusTrain { get; set; }

        [InverseProperty(nameof(Schedule.Train))]
        public ICollection<Schedule> Schedules { get; set; }

        public Train()
        {
        }

        public Train(int id)
        {
            IdTrain = id;
        }

        public Train(TrainView trainView)
        {
            IdTrain = trainView.IdTrain;
            CodeTrain = trainView.CodeTrain;
            NameTrain = trainView.NameTrain;
            Ac1Seat = trainView.Ac1Seat;
            Ac2Seat = trainView.Ac2Seat;
            Ac3Seat = trainView.Ac3Seat;
            FcSeat = trainView.FcSeat;
            ScSeat = trainView.ScSeat;
            StatusTrain = trainView.StatusTrain;
        }
    }
}
