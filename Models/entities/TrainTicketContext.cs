﻿using Microsoft.EntityFrameworkCore;

namespace DemoAspNetCore.Models.entities
{
    public class TrainTicketContext : DbContext
    {
        public TrainTicketContext() 
        {

        }
        public TrainTicketContext(DbContextOptions<TrainTicketContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            //DbConnect.GenerateKeys();
            //optionsBuilder.UseSqlServer("Server=DESKTOP-3SUF34M;Database=TrainTicket; Trusted_Connection = True; MultipleActiveResultSets = true;TrustServerCertificate=True");
            optionsBuilder.UseSqlServer(DbConnect.Decrypt());
            
        }

        public DbSet<Admin> Admins { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Fare> Fares { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Train> Trains { get; set; }
    }
}
