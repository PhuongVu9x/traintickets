﻿
function onChangeStatus(e, action) {
    const id = e.name
    const status = e.checked
    const data = { id, status };
    // Setting.
    $.ajax({
        type: "POST",
        url: action,
        data: data,
        dataType: "json",
        success: function (result) {
            // Result.
            $("#result").get(0).text = result;
            $("#result").get(0).innerHTML = result;
            setTimeout(removeResult, 3000)
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //do your own thing
            $("#result").get(0).text = "Fail";
        }
    });
}