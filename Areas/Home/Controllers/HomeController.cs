﻿using DemoAspNetCore.Models.Business;
using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Facebook;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.Web.CodeGenerators.Mvc.Controller;

namespace DemoAspNetCore.Areas.Home.Controllers
{
    [Area(nameof(Home))]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.Tickets = ModelViewMaker.Instance.GetTickets();
            ViewBag.Stations = ModelViewMaker.Instance.GetStations();
            ViewBag.Fares = ModelViewMaker.Instance.GetFares();
            ViewBag.Schedules = ModelViewMaker.Instance.GetSchedules();
            ViewBag.Trains = ModelViewMaker.Instance.GetTrains();
            ViewBag.Classes = ModelViewMaker.Instance.GetClasses();

            if (HttpContext.Session.GetString(EventMgr.Customer.LOGIN_CUSTOMER) != null)
            {
                var data = AppMgr.GetJson<Customer>(HttpContext.Session.GetString(EventMgr.Customer.LOGIN_CUSTOMER));
                if (data != null) ViewBag.Customer = data;
            }

            return View();
        }

        [HttpGet]
        [Route("/Home/Register")]
        public IActionResult Register()
        {
            ViewBag.Result = HttpContext.Session.GetString(EventMgr.Customer.CREATE_ACCOUNT_CUSTOMER);
            HttpContext.Session.Remove(EventMgr.Customer.CREATE_ACCOUNT_CUSTOMER);
            return View();
        }

        [HttpPost]
        [Route("/Home/CustomerRegister")]
        public IActionResult CustomerRegister(Customer customer)
        {
            customer.TokenCustomer = AppMgr.EncryptASCII(AppMgr.GeneratorToken());
            var result = ModelMaker.Instance.Insert(customer);

            if (result.Equals(AppMgr.Successfull))
            {
                var email = new Email();
                result = email.SendMailCredential(customer.EmailCustomer, customer.TokenCustomer);
                if (result.Equals("Failed"))
                    HttpContext.Session.SetString(EventMgr.Customer.RESULT_CRUD_CUSTOMER, AppMgr.EmailError);
                else
                    HttpContext.Session.SetString(EventMgr.Customer.RESULT_CRUD_CUSTOMER, AppMgr.Successfull);
            }
            else
                HttpContext.Session.SetString(EventMgr.Customer.RESULT_CRUD_CUSTOMER, AppMgr.ServerError);

            return RedirectToAction(nameof(Register));
        }

        [HttpGet]
        [Route("/Home/Login")]
        public IActionResult Login()
        {
            ViewBag.Result = HttpContext.Session.GetString(EventMgr.Customer.LOGIN_RESULT_CUSTOMER);
            if (HttpContext.Session.GetString(EventMgr.Customer.LOGIN_CUSTOMER) != null)
            {
                var data = AppMgr.GetJson<Customer>(HttpContext.Session.GetString(EventMgr.Customer.LOGIN_CUSTOMER));
                if (data != null) ViewBag.Customer = data;
            }

            HttpContext.Session.Remove(EventMgr.Customer.LOGIN_RESULT_CUSTOMER);
            return View();
        }

        [HttpGet]
        [Route("/Home/Profile")]
        public IActionResult Profile()
        {
            if (HttpContext.Session.GetString(EventMgr.Customer.LOGIN_CUSTOMER) != null)
            {
                var data = AppMgr.GetJson<Customer>(HttpContext.Session.GetString(EventMgr.Customer.LOGIN_CUSTOMER));
                if (data != null) ViewBag.Customer = data;
            }
            return View();
        }

        [HttpPost]
        [Route("/Home/CustomerLogin")]
        public IActionResult CustomerLogin(Customer param)
        {
            var customer = ModelMaker.Instance.Get(param);

            if (param.LoginStatus == 2 || param.LoginStatus == 3)
            {
                if (customer is null)
                {
                    param.EmailCustomer = "";
                    param.PasswordCustomer = "111111";
                    param.ContactCustomer = "0123456789";
                    param.DobCustomer = "01/01/1979";
                    param.TokenCustomer = "";
                    if (param.LoginStatus == 2) param.FacebookId = "";
                    else param.GoogleId = "";

                    ModelMaker.Instance.Insert(param);
                    customer = ModelMaker.Instance.Get(param);
                }
            }

            if (customer is null)
                HttpContext.Session.SetString(EventMgr.Customer.LOGIN_RESULT_CUSTOMER, AppMgr.Fail);
            else
            {
                HttpContext.Session.SetString(EventMgr.Customer.LOGIN_RESULT_CUSTOMER, AppMgr.Successfull);
                HttpContext.Session.SetString(EventMgr.Customer.LOGIN_CUSTOMER, AppMgr.ToJson(customer));
            }

            return RedirectToAction(nameof(Login));
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Remove(EventMgr.Customer.LOGIN_CUSTOMER);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Route("/Home/LoginFireBase")]
        public bool LoginFireBase(Customer param)
        {
            if (!ModelMaker.Instance.IsExists(param))
            {
                param.EmailCustomer = "";
                param.PasswordCustomer = "111111";
                param.ContactCustomer = "0123456789";
                param.DobCustomer = "01/01/1979";
                param.TokenCustomer = "";
                param.StatusCustomer = true;
                if (param.LoginStatus == 2) param.FacebookId = "";
                else param.GoogleId = "";

                ModelMaker.Instance.Insert(param);
            }
            var customer = ModelMaker.Instance.Get(param);

            HttpContext.Session.SetString(EventMgr.Customer.LOGIN_RESULT_CUSTOMER, AppMgr.Successfull);
            HttpContext.Session.SetString(EventMgr.Customer.LOGIN_CUSTOMER, AppMgr.ToJson(customer));
            return true;
        }

        [HttpPost]
        [Route("/Home/CustomerUpdate")]
        public IActionResult CustomerUpdate(Customer param)
        {
            //var customer = ModelMaker.Instance.Get(param);

            //if (customer is null)
            //    HttpContext.Session.SetString(EventMgr.Customer.LOGIN_RESULT_CUSTOMER, AppMgr.Fail);
            //else
            //{
            //    HttpContext.Session.SetString(EventMgr.Customer.LOGIN_RESULT_CUSTOMER, AppMgr.Successfull);
            //    HttpContext.Session.SetString(EventMgr.Customer.LOGIN_CUSTOMER, AppMgr.ToJson(customer));
            //}

            return RedirectToAction(nameof(Profile));
        }

        [HttpPost]
        [Route("/Home/CustomerRenewSession")]
        public string CustomerRenewSession(Customer param)
        {
            HttpContext.Session.SetString(EventMgr.Customer.RENEW_SESSION_CUSTOMER, AppMgr.ToJson(param));
            return AppMgr.ToJson(true);
        }

        [HttpPost]
        [Route("/Ad/Admin/CustomerCheck")]
        public string CustomerCheck(string email)
        {
            var result = ModelMaker.Instance.Check(new Customer(email));
            return AppMgr.ToJson(result);
        }

        [HttpGet]
        [Route("/Home/PaymentView")]
        public IActionResult PaymentView()
        {
            return View();
        }

        [HttpPost]
        [Route("/Home/TicketCreate")]
        public IActionResult TicketCreate(string param)
        {
            //if (param.Contains("null"))
            //{
            //    HttpContext.Session.SetString(EventMgr.Ticket.RESULT_CRUD_TICKET, AppMgr.Fail);
            //    return RedirectToAction(nameof(Index));
            //}

            //var tickets = AppMgr.GetJson<List<Ticket>>(param);
            //foreach (var item in tickets) 
            //    ModelMaker.Instance.Insert(item);

            //HttpContext.Session.SetString(EventMgr.Ticket.RESULT_CRUD_TICKET, AppMgr.Successfull);
           
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Route("/Home/Payment")]
        public IActionResult Payment(string param)
        {
            string url = "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
            string returnUrl = "https://localhost:7256/Home/PaymentConfirm";
            string tmnCode = "ST5OX7M6";
            string hashSecret = "NBHVKKFJZIDRRZFXFGMKCQHFEDPMNHCK";

            var pay = new OnlinePay();

            pay.AddRequestData("vnp_Version", "2.1.0"); //Phiên bản api mà merchant kết nối. Phiên bản hiện tại là 2.1.0
            pay.AddRequestData("vnp_Command", "pay"); //Mã API sử dụng, mã cho giao dịch thanh toán là 'pay'
            pay.AddRequestData("vnp_TmnCode", tmnCode); //Mã website của merchant trên hệ thống của VNPAY (khi đăng ký tài khoản sẽ có trong mail VNPAY gửi về)
            pay.AddRequestData("vnp_Amount", "1000000"); //số tiền cần thanh toán, công thức: số tiền * 100 - ví dụ 10.000 (mười nghìn đồng) --> 1000000
            pay.AddRequestData("vnp_BankCode", ""); //Mã Ngân hàng thanh toán (tham khảo: https://sandbox.vnpayment.vn/apis/danh-sach-ngan-hang/), có thể để trống, người dùng có thể chọn trên cổng thanh toán VNPAY
            pay.AddRequestData("vnp_CreateDate", DateTime.Now.ToString("yyyyMMddHHmmss")); //ngày thanh toán theo định dạng yyyyMMddHHmmss
            pay.AddRequestData("vnp_CurrCode", "VND"); //Đơn vị tiền tệ sử dụng thanh toán. Hiện tại chỉ hỗ trợ VND
            pay.AddRequestData("vnp_IpAddr", "127.0.0.1"); //Địa chỉ IP của khách hàng thực hiện giao dịch
            pay.AddRequestData("vnp_Locale", "vn"); //Ngôn ngữ giao diện hiển thị - Tiếng Việt (vn), Tiếng Anh (en)
            pay.AddRequestData("vnp_OrderInfo", "Thanh toan don hang"); //Thông tin mô tả nội dung thanh toán
            pay.AddRequestData("vnp_OrderType", "other"); //topup: Nạp tiền điện thoại - billpayment: Thanh toán hóa đơn - fashion: Thời trang - other: Thanh toán trực tuyến
            pay.AddRequestData("vnp_ReturnUrl", returnUrl); //URL thông báo kết quả giao dịch khi Khách hàng kết thúc thanh toán
            pay.AddRequestData("vnp_TxnRef", DateTime.Now.Ticks.ToString()); //mã hóa đơn

            string paymentUrl = pay.CreateRequestUrl(url, hashSecret);

            return Redirect(paymentUrl);
        }

        public ActionResult PaymentConfirm()
        {
            Console.WriteLine(Request.QueryString.Value);
            //if (Request.QueryString.Value > 0)
            //{
            //    string hashSecret = ConfigurationManager.AppSettings["HashSecret"]; //Chuỗi bí mật
            //    var vnpayData = Request.QueryString;
            //    PayLib pay = new PayLib();

            //    //lấy toàn bộ dữ liệu được trả về
            //    foreach (string s in vnpayData)
            //    {
            //        if (!string.IsNullOrEmpty(s) && s.StartsWith("vnp_"))
            //        {
            //            pay.AddResponseData(s, vnpayData[s]);
            //        }
            //    }

            //    long orderId = Convert.ToInt64(pay.GetResponseData("vnp_TxnRef")); //mã hóa đơn
            //    long vnpayTranId = Convert.ToInt64(pay.GetResponseData("vnp_TransactionNo")); //mã giao dịch tại hệ thống VNPAY
            //    string vnp_ResponseCode = pay.GetResponseData("vnp_ResponseCode"); //response code: 00 - thành công, khác 00 - xem thêm https://sandbox.vnpayment.vn/apis/docs/bang-ma-loi/
            //    string vnp_SecureHash = Request.QueryString["vnp_SecureHash"]; //hash của dữ liệu trả về

            //    bool checkSignature = pay.ValidateSignature(vnp_SecureHash, hashSecret); //check chữ ký đúng hay không?

            //    if (checkSignature)
            //    {
            //        if (vnp_ResponseCode == "00")
            //        {
            //            //Thanh toán thành công
            //            ViewBag.Message = "Thanh toán thành công hóa đơn " + orderId + " | Mã giao dịch: " + vnpayTranId;
            //        }
            //        else
            //        {
            //            //Thanh toán không thành công. Mã lỗi: vnp_ResponseCode
            //            ViewBag.Message = "Có lỗi xảy ra trong quá trình xử lý hóa đơn " + orderId + " | Mã giao dịch: " + vnpayTranId + " | Mã lỗi: " + vnp_ResponseCode;
            //        }
            //    }
            //    else
            //    {
            //        ViewBag.Message = "Có lỗi xảy ra trong quá trình xử lý";
            //    }
            //}

            return View();
        }

        [HttpGet]
        public ActionResult ActivateEmail(string email, string token)
        {
            var customer = ModelMaker.Instance.Get(new Customer(AppMgr.DecryptASCII(email), token));
            customer.TokenCustomer = "";
            customer.StatusCustomer = true;
            ModelMaker.Instance.Update(customer);

            return RedirectToAction(nameof(Login));
        }


        //public async Task<IActionResult> LogoutFireBase()
        //{
        //    HttpContext.Session.Remove(EventMgr.Customer.LOGIN_CUSTOMER);
        //    await AuthenticationHttpContextExtensions.SignOutAsync(HttpContext, CookieAuthenticationDefaults.AuthenticationScheme);
        //    await HttpContext.SignOutAsync("Cookies");

        //    await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        //    HttpContext.Response.Cookies.Delete(".AspNetCore.Cookies");
        //    await HttpContext.SignOutAsync();
        //    return RedirectToAction(nameof(Login));
        //}

        //public IActionResult GoogleLoginView()
        //{
        //    //await HttpContext.ChallengeAsync(GoogleDefaults.AuthenticationScheme, new AuthenticationProperties()
        //    //{
        //    //    RedirectUri = Url.Action(nameof(GoogleResponse))
        //    //});
        //    var properties = new AuthenticationProperties { RedirectUri = Url.Action(nameof(GoogleResponse)) };
        //    return Challenge(properties, GoogleDefaults.AuthenticationScheme);
        //}

        //public async Task<IActionResult> GoogleResponse()
        //{
        //    var result = await HttpContext.AuthenticateAsync(CookieAuthenticationDefaults.AuthenticationScheme);

        //    var claims = result.Principal.Identities.First().Claims.Select(claim => new
        //    {
        //        claim.Issuer,
        //        claim.OriginalIssuer,
        //        claim.Type,
        //        claim.Value
        //    });

        //    if (User.Identity.IsAuthenticated)
        //    {
        //        return RedirectToAction(nameof(Login));
        //    }
        //    else
        //    {
        //        return Json(claims);
        //    }
        //}
    }
}
