﻿using DemoAspNetCore.Models.Business;
using DemoAspNetCore.Models.designPatterns;
using DemoAspNetCore.Models.entities;
using DemoAspNetCore.Models.modelViews;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace DemoAspNetCore.Areas.Ad.Controllers
{
    [Area(nameof(Ad))]
    public class AdminController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Content = "Coming soon";
            return View();
        }

        #region Admin
        [HttpGet]
        [Route("/Ad/Admin/AdminView")]
        public IActionResult AdminView()
        {
            ViewBag.Title = "Admin View";
            ViewBag.Admins = ModelViewMaker.Instance.GetAdmins();

            if(HttpContext.Session.GetString(EventMgr.Admin.SEARCH_ADMIN) != null)
            {
                var data = AppMgr.GetJson<List<AdminView>>(HttpContext.Session.GetString(EventMgr.Admin.SEARCH_ADMIN));
                if (data != null) ViewBag.Admins = data;
            }

            ViewBag.Result = HttpContext.Session.GetString(EventMgr.Admin.RESULT_CRUD_ADMIN);
            HttpContext.Session.Remove(EventMgr.Admin.RESULT_CRUD_ADMIN);
            HttpContext.Session.Remove(EventMgr.Admin.SEARCH_ADMIN);

            return View();
        }

        [HttpPost]
        [Route("/Ad/Admin/AdminCreate")]
        public IActionResult AdminCreate(Admin param)
        {
            var result = ModelMaker.Instance.Insert(param);
            HttpContext.Session.SetString(EventMgr.Admin.RESULT_CRUD_ADMIN, result);
            return RedirectToAction(nameof(AdminView));
        }

        [HttpPost]
        [Route("/Ad/Admin/AdminUpdate")]
        public IActionResult AdminUpdate(Admin param)
        {
            param.StatusAdmin = ModelMaker.Instance.Get(param).StatusAdmin;
            var result = ModelMaker.Instance.Update(param);
            HttpContext.Session.SetString(EventMgr.Admin.RESULT_CRUD_ADMIN, result);
            return RedirectToAction(nameof(AdminView));
        }

        [HttpPost]
        [Route("/Ad/Admin/AdminStatus")]
        public string AdminStatus(int id, bool status)
        {
            var param = ModelMaker.Instance.Get(new Admin(id));
            param.StatusAdmin = status;
            var result = ModelMaker.Instance.ChangeStatus(param);
            return AppMgr.ToJson(result);
        }

        [HttpPost]
        [Route("/Ad/Admin/AdminSearch")]
        public IActionResult AdminSearch(string searchText)
        {
            var result = ModelViewMaker.Instance.GetAdmins();
            if (!searchText.IsNullOrEmpty())
                result = ModelViewMaker.Instance.Search(new AdminView(searchText));
            HttpContext.Session.SetString(EventMgr.Admin.SEARCH_ADMIN, AppMgr.ToJson(result));
            return RedirectToAction(nameof(AdminView));
        }
        #endregion

        #region Station

        [HttpGet]
        [Route("/Ad/Admin/StationView")]
        public IActionResult StationView()
        {
            ViewBag.Title = "Station View";
            ViewBag.Stations = ModelViewMaker.Instance.GetStations();

            if (HttpContext.Session.GetString(EventMgr.Station.SEARCH_STATION) != null)
            {
                var data = AppMgr.GetJson<List<StationView>>(HttpContext.Session.GetString(EventMgr.Station.SEARCH_STATION));
                if (data != null) ViewBag.Stations = data;
            }

            ViewBag.Result = HttpContext.Session.GetString(EventMgr.Station.RESULT_CRUD_STATION);
            HttpContext.Session.Remove(EventMgr.Station.RESULT_CRUD_STATION);
            HttpContext.Session.Remove(EventMgr.Station.SEARCH_STATION);
            return View();
        }

        [HttpPost]
        [Route("/Ad/Admin/StationCreate")]
        public IActionResult StationCreate(Station param)
        {
            var result = ModelMaker.Instance.Insert(param);
            HttpContext.Session.SetString(EventMgr.Station.RESULT_CRUD_STATION, result);
            return RedirectToAction(nameof(StationView));
        }

        [HttpPost]
        [Route("/Ad/Admin/StationUpdate")]
        public IActionResult StationUpdate(Station param)
        {
            param.StatusStation = ModelMaker.Instance.Get(param).StatusStation;
            var result = ModelMaker.Instance.Update(param);
            HttpContext.Session.SetString(EventMgr.Station.RESULT_CRUD_STATION, result);
            return RedirectToAction(nameof(StationView));
        }

        [HttpPost]
        [Route("/Ad/Admin/StationStatus")]
        public string StationStatus(int id, bool status)
        {
            var param = ModelMaker.Instance.Get(new Station(id));
            param.StatusStation = status;
            var result = ModelMaker.Instance.ChangeStatus(param);
            return AppMgr.ToJson(result);
        }

        [HttpPost]
        [Route("/Ad/Admin/StationSearch")]
        public IActionResult StationSearch(string searchText)
        {
            var result = ModelViewMaker.Instance.GetStations();
            if (!searchText.IsNullOrEmpty())
                result = ModelViewMaker.Instance.Search(new StationView(searchText));
            HttpContext.Session.SetString(EventMgr.Station.SEARCH_STATION, AppMgr.ToJson(result));
            return RedirectToAction(nameof(StationView));
        }

        #endregion

        #region Train
        [HttpGet]
        [Route("/Ad/Admin/TrainView")]
        public IActionResult TrainView()
        {
            ViewBag.Title = "Train View";
            ViewBag.Trains = ModelViewMaker.Instance.GetTrains();

            if (HttpContext.Session.GetString(EventMgr.Train.SEARCH_TRAIN) != null)
            {
                var data = AppMgr.GetJson<List<TrainView>>(HttpContext.Session.GetString(EventMgr.Train.SEARCH_TRAIN));
                if (data != null) ViewBag.Trains = data;
            }

            ViewBag.Result = HttpContext.Session.GetString(EventMgr.Train.RESULT_CRUD_TRAIN);
            HttpContext.Session.Remove(EventMgr.Train.RESULT_CRUD_TRAIN);
            HttpContext.Session.Remove(EventMgr.Train.SEARCH_TRAIN);
            return View();
        }

        [HttpPost]
        [Route("/Ad/Admin/TrainCreate")]
        public IActionResult TrainCreate(Train param)
        {
            var result = ModelMaker.Instance.Insert(param);
            HttpContext.Session.SetString(EventMgr.Train.RESULT_CRUD_TRAIN, result);
            return RedirectToAction(nameof(TrainView));
        }

        [HttpPost]
        [Route("/Ad/Admin/TrainUpdate")]
        public IActionResult TrainUpdate(Train param)
        {
            param.StatusTrain = ModelMaker.Instance.Get(param).StatusTrain;
            var result = ModelMaker.Instance.Update(param);
            HttpContext.Session.SetString(EventMgr.Train.RESULT_CRUD_TRAIN, result);
            return RedirectToAction(nameof(TrainView));
        }

        [HttpPost]
        [Route("/Ad/Admin/TrainStatus")]
        public string TrainStatus(int id, bool status)
        {
            var param = ModelMaker.Instance.Get(new Train(id));
            param.StatusTrain = status;
            var result = ModelMaker.Instance.ChangeStatus(param);
            return AppMgr.ToJson(result);
        }

        [HttpPost]
        [Route("/Ad/Admin/TrainSearch")]
        public IActionResult TrainSearch(string searchText)
        {
            var result = ModelViewMaker.Instance.GetTrains();
            if (!searchText.IsNullOrEmpty())
                result = ModelViewMaker.Instance.Search(new TrainView(searchText));
            HttpContext.Session.SetString(EventMgr.Train.SEARCH_TRAIN, AppMgr.ToJson(result));
            return RedirectToAction(nameof(TrainView));
        }
        #endregion

        #region Class
        [HttpGet]
        [Route("/Ad/Admin/ClassView")]
        public IActionResult ClassView()
        {
            ViewBag.Title = "Class View";
            ViewBag.Classes = ModelViewMaker.Instance.GetClasses();

            if (HttpContext.Session.GetString(EventMgr.Class.SEARCH_CLASS) != null)
            {
                var data = AppMgr.GetJson<List<ClassView>>(HttpContext.Session.GetString(EventMgr.Class.SEARCH_CLASS));
                if (data != null) ViewBag.Classes = data;
            }

            ViewBag.Result = HttpContext.Session.GetString(EventMgr.Class.RESULT_CRUD_CLASS);
            HttpContext.Session.Remove(EventMgr.Class.RESULT_CRUD_CLASS);
            HttpContext.Session.Remove(EventMgr.Class.SEARCH_CLASS);
            return View();
        }

        [HttpPost]
        [Route("/Ad/Admin/ClassCreate")]
        public IActionResult ClassCreate(Class param)
        {
            var result = ModelMaker.Instance.Insert(param);
            HttpContext.Session.SetString(EventMgr.Class.RESULT_CRUD_CLASS, result);
            return RedirectToAction(nameof(ClassView));
        }

        [HttpPost]
        [Route("/Ad/Admin/ClassUpdate")]
        public IActionResult ClassUpdate(Class param)
        {
            param.StatusClass = ModelMaker.Instance.Get(param).StatusClass;
            var result = ModelMaker.Instance.Update(param);
            HttpContext.Session.SetString(EventMgr.Class.RESULT_CRUD_CLASS, result);
            return RedirectToAction(nameof(ClassView));
        }

        [HttpPost]
        [Route("/Ad/Admin/ClassStatus")]
        public string ClassStatus(int id, bool status)
        {
            var param = ModelMaker.Instance.Get(new Class(id));
            param.StatusClass = status;
            var result = ModelMaker.Instance.ChangeStatus(param);
            return AppMgr.ToJson(result);
        }

        [HttpPost]
        [Route("/Ad/Admin/ClassSearch")]
        public IActionResult ClassSearch(string searchText)
        {
            var result = ModelViewMaker.Instance.GetClasses();
            if (!searchText.IsNullOrEmpty())
                result = ModelViewMaker.Instance.Search(new ClassView(searchText));
            HttpContext.Session.SetString(EventMgr.Class.SEARCH_CLASS, AppMgr.ToJson(result));
            return RedirectToAction(nameof(ClassView));
        }
        #endregion

        #region Fare
        [HttpGet]
        [Route("/Ad/Admin/FareView")]
        public IActionResult FareView()
        {
            ViewBag.Title = "Fare View";
            ViewBag.Fares = ModelViewMaker.Instance.GetFares();
            ViewBag.Stations = ModelMaker.Instance.GetStations();
            if (HttpContext.Session.GetString(EventMgr.Fare.SEARCH_FARE) != null)
            {
                var data = AppMgr.GetJson<List<FareView>>(HttpContext.Session.GetString(EventMgr.Fare.SEARCH_FARE));
                if (data != null) ViewBag.Fares = data;
            }

            ViewBag.Result = HttpContext.Session.GetString(EventMgr.Fare.RESULT_CRUD_FARE);
            HttpContext.Session.Remove(EventMgr.Fare.RESULT_CRUD_FARE);
            HttpContext.Session.Remove(EventMgr.Fare.SEARCH_FARE);
            return View();
        }

        [HttpPost]
        [Route("/Ad/Admin/FareCreate")]
        public IActionResult FareCreate(Fare param)
        {
            var result = ModelMaker.Instance.Insert(param);
            HttpContext.Session.SetString(EventMgr.Fare.RESULT_CRUD_FARE, result);
            return RedirectToAction(nameof(FareView));
        }

        [HttpPost]
        [Route("/Ad/Admin/FareUpdate")]
        public IActionResult FareUpdate(Fare param)
        {
            param.StatusFare = ModelMaker.Instance.Get(param).StatusFare;
            var result = ModelMaker.Instance.Update(param);
            HttpContext.Session.SetString(EventMgr.Fare.RESULT_CRUD_FARE, result);
            return RedirectToAction(nameof(FareView));
        }

        [HttpPost]
        [Route("/Ad/Admin/FareStatus")]
        public string FareStatus(int id, bool status)
        {
            var param = ModelMaker.Instance.Get(new Fare(id));
            param.StatusFare = status;
            var result = ModelMaker.Instance.ChangeStatus(param);
            return AppMgr.ToJson(result);
        }

        [HttpPost]
        [Route("/Ad/Admin/FareSearch")]
        public IActionResult FareSearch(string searchText)
        {
            var result = ModelViewMaker.Instance.GetFares();
            if (!searchText.IsNullOrEmpty())
                result = ModelViewMaker.Instance.Search(new FareView(searchText));
            HttpContext.Session.SetString(EventMgr.Fare.SEARCH_FARE, AppMgr.ToJson(result));
            return RedirectToAction(nameof(FareView));
        }
        #endregion

        #region Schedule
        [HttpGet]
        [Route("/Ad/Admin/ScheduleView")]
        public IActionResult ScheduleView()
        {
            ViewBag.Title = "Schedule View";
            ViewBag.Schedules = ModelViewMaker.Instance.GetSchedules();
            ViewBag.FaresView = ModelViewMaker.Instance.GetFares();
            ViewBag.Trains = ModelMaker.Instance.GetTrains();
            if (HttpContext.Session.GetString(EventMgr.Schedule.SEARCH_SCHEDULE) != null)
            {
                var data = AppMgr.GetJson<List<ScheduleView>>(HttpContext.Session.GetString(EventMgr.Schedule.SEARCH_SCHEDULE));
                if (data != null) ViewBag.Schedules = data;
            }

            ViewBag.Result = HttpContext.Session.GetString(EventMgr.Schedule.RESULT_CRUD_SCHEDULE);
            HttpContext.Session.Remove(EventMgr.Schedule.RESULT_CRUD_SCHEDULE);
            HttpContext.Session.Remove(EventMgr.Schedule.SEARCH_SCHEDULE);
            return View();
        }

        [HttpPost]
        [Route("/Ad/Admin/ScheduleCreate")]
        public IActionResult ScheduleCreate(Schedule param)
        {
            var result = ModelMaker.Instance.Insert(param);
            HttpContext.Session.SetString(EventMgr.Schedule.RESULT_CRUD_SCHEDULE, result);
            return RedirectToAction(nameof(ScheduleView));
        }

        [HttpPost]
        [Route("/Ad/Admin/ScheduleUpdate")]
        public IActionResult ScheduleUpdate(Schedule param)
        {
            param.StatusSchedule = ModelMaker.Instance.Get(param).StatusSchedule;
            var result = ModelMaker.Instance.Update(param);
            HttpContext.Session.SetString(EventMgr.Schedule.RESULT_CRUD_SCHEDULE, result);
            return RedirectToAction(nameof(ScheduleView));
        }

        [HttpPost]
        [Route("/Ad/Admin/ScheduleStatus")]
        public string ScheduleStatus(int id, bool status)
        {
            var param = ModelMaker.Instance.Get(new Schedule(id));
            param.StatusSchedule = status;
            var result = ModelMaker.Instance.ChangeStatus(param);
            return AppMgr.ToJson(result);
        }

        [HttpPost]
        [Route("/Ad/Admin/ScheduleSearch")]
        public IActionResult ScheduleSearch(string searchText)
        {
            var result = ModelViewMaker.Instance.GetSchedules();
            if (!searchText.IsNullOrEmpty())
                result = ModelViewMaker.Instance.Search(new ScheduleView(searchText));
            HttpContext.Session.SetString(EventMgr.Schedule.SEARCH_SCHEDULE, AppMgr.ToJson(result));
            return RedirectToAction(nameof(ScheduleView));
        }

        #endregion

        #region Customer
        [HttpGet]
        [Route("/Ad/Admin/CustomerView")]
        public IActionResult CustomerView()
        {
            ViewBag.Title = "Customer View";
            ViewBag.Customers = ModelViewMaker.Instance.GetCustomers();
            if (HttpContext.Session.GetString(EventMgr.Customer.SEARCH_CUSTOMER) != null)
            {
                var data = AppMgr.GetJson<List<CustomerView>>(HttpContext.Session.GetString(EventMgr.Customer.SEARCH_CUSTOMER));
                if (data != null) ViewBag.Customers = data;
            }

            ViewBag.Result = HttpContext.Session.GetString(EventMgr.Customer.RESULT_CRUD_CUSTOMER);
            HttpContext.Session.Remove(EventMgr.Customer.RESULT_CRUD_CUSTOMER);
            HttpContext.Session.Remove(EventMgr.Customer.SEARCH_CUSTOMER);
            return View();
        }

        [HttpPost]
        [Route("/Ad/Admin/CustomerCreate")]
        public IActionResult CustomerCreate(Customer param)
        {
            var result = ModelMaker.Instance.Insert(param);
            HttpContext.Session.SetString(EventMgr.Customer.RESULT_CRUD_CUSTOMER, result);
            return RedirectToAction(nameof(CustomerView));
        }

        [HttpPost]
        [Route("/Ad/Admin/CustomerUpdate")]
        public IActionResult CustomerUpdate(Customer param)
        {
            var oldData = ModelMaker.Instance.Get(param);
            param.TokenCustomer = oldData.TokenCustomer;
            param.AvatarCustomer = oldData.AvatarCustomer;
            param.PasswordCustomer = oldData.PasswordCustomer;
            param.StatusCustomer = oldData.StatusCustomer;
            var result = ModelMaker.Instance.Update(param);
            HttpContext.Session.SetString(EventMgr.Customer.RESULT_CRUD_CUSTOMER, result);
            return RedirectToAction(nameof(CustomerView));
        }

        [HttpPost]
        [Route("/Ad/Admin/CustomerStatus")]
        public string CustomerStatus(int id,bool status)
        {
            var param = ModelMaker.Instance.Get(new Customer(id));
            param.StatusCustomer = status;
            var result = ModelMaker.Instance.ChangeStatus(param);
            return AppMgr.ToJson(result);
        }

        [HttpPost]
        [Route("/Ad/Admin/CustomerSearch")]
        public IActionResult CustomerSearch(string searchText)
        {
            var result = ModelViewMaker.Instance.GetCustomers();
            if (!searchText.IsNullOrEmpty())
                result = ModelViewMaker.Instance.Search(new CustomerView(searchText));
            HttpContext.Session.SetString(EventMgr.Customer.SEARCH_CUSTOMER, AppMgr.ToJson(result));
            return RedirectToAction(nameof(CustomerView));
        }

        [HttpPost]
        [Route("/Ad/Admin/CustomerInfoSearch")]
        public string CustomerInfoSearch(string searchText)
        {
            var result = ModelViewMaker.Instance.GetCustomers();
            if (!searchText.IsNullOrEmpty())
                result = ModelViewMaker.Instance.Search(new CustomerView(searchText));
            return AppMgr.ToJson(result);
        }


        #endregion

        #region Ticket
        [HttpGet]
        [Route("/Ad/Admin/TicketView")]
        public IActionResult TicketView()
        {
            ViewBag.Title = "Ticket View";
            ViewBag.Tickets = ModelViewMaker.Instance.GetTickets();
            ViewBag.Stations = ModelViewMaker.Instance.GetStations();
            ViewBag.Fares = ModelViewMaker.Instance.GetFares();
            ViewBag.Schedules = ModelViewMaker.Instance.GetSchedules();
            ViewBag.Trains = ModelViewMaker.Instance.GetTrains();
            ViewBag.Classes = ModelViewMaker.Instance.GetClasses();
            ViewBag.Customers = ModelViewMaker.Instance.GetCustomers();

            if (HttpContext.Session.GetString(EventMgr.Ticket.SEARCH_TICKET) != null)
            {
                var data = AppMgr.GetJson<List<TicketView>>(HttpContext.Session.GetString(EventMgr.Ticket.SEARCH_TICKET));
                if (data != null) ViewBag.Tickets = data;
            }
            
            ViewBag.Result = HttpContext.Session.GetString(EventMgr.Ticket.RESULT_CRUD_TICKET);
            HttpContext.Session.Remove(EventMgr.Ticket.RESULT_CRUD_TICKET);
            HttpContext.Session.Remove(EventMgr.Ticket.SEARCH_TICKET);
            //PartialView("MyPartialView", new Admin());
            return View();
        }

        [HttpPost]
        [Route("/Ad/Admin/TicketCreate")]
        public IActionResult TicketCreate(string param)
        {
            if (param.Contains("null"))
            {
                HttpContext.Session.SetString(EventMgr.Ticket.RESULT_CRUD_TICKET, AppMgr.Fail);
                return RedirectToAction(nameof(TicketView));
            }

            var tickets = AppMgr.GetJson<List<Ticket>>(param);
            foreach (var item in tickets)
                ModelMaker.Instance.Insert(item);

            HttpContext.Session.SetString(EventMgr.Ticket.RESULT_CRUD_TICKET, AppMgr.Successfull);
            return RedirectToAction(nameof(TicketView));
        }

        [HttpPost]
        [Route("/Ad/Admin/TicketUpdate")]
        public IActionResult TicketUpdate(Ticket param)
        {
            //var oldData = ModelMaker.Instance.Get(param);
            //param.AvatarCustomer = oldData.AvatarCustomer;
            //param.PasswordCustomer = oldData.PasswordCustomer;
            //param.StatusCustomer = oldData.StatusCustomer;
            var result = ModelMaker.Instance.Update(param);
            HttpContext.Session.SetString(EventMgr.Ticket.RESULT_CRUD_TICKET, result);
            return RedirectToAction(nameof(TicketView));
        }

        //[HttpPost]
        //[Route("/Ad/Admin/TicketStatus")]
        //public string TicketStatus(int id,bool status)
        //{
        //    var param = ModelMaker.Instance.Get(new Ticket(id));
        //    param.StatusTicket = status;
        //    var result = ModelMaker.Instance.ChangeStatus(param);
        //    return AppMgr.ToJson(result);
        //}

        [HttpPost]
        [Route("/Ad/Admin/TicketSearch")]
        public IActionResult TicketSearch(string searchText)
        {
            var result = ModelViewMaker.Instance.GetTickets();
            if (!searchText.IsNullOrEmpty())
                result = ModelViewMaker.Instance.Search(new TicketView(searchText));
            HttpContext.Session.SetString(EventMgr.Ticket.SEARCH_TICKET, AppMgr.ToJson(result));
            return RedirectToAction(nameof(TicketView));
        }

        [HttpPost]
        [Route("/Ad/Admin/CheckTickket")]
        public string CheckTickket(Ticket param)
        {
            var result = ModelMaker.Instance.Check(param);
            return AppMgr.ToJson(result);
        }

        #endregion
    }
}
