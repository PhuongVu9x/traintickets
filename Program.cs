using DemoAspNetCore.Models.entities;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Facebook;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

//Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddDistributedMemoryCache();
builder.Services.AddDbContext<TrainTicketContext>(options => options.UseSqlServer(DbConnect.Decrypt()));
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(15);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});
builder.Services.AddAuthentication(option =>
{
    option.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    option.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
}).AddCookie().AddGoogle(GoogleDefaults.AuthenticationScheme,option =>
{
    option.ClientId = "953188150325-1i9etme464tarif0f2omidpkh9fvddrv.apps.googleusercontent.com";
    option.ClientSecret = "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";
    option.CallbackPath = "/GoogleLoginView";
}).AddFacebook(FacebookDefaults.AuthenticationScheme, option =>
{
    option.AppId = "233294979060731";
    option.AppSecret = "c821971a0d93aae3c4ab523271defa7d";
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();
app.UseAuthentication();

app.UseSession();

app.MapAreaControllerRoute(
    name: "Client",
    areaName: "Home",
    pattern: "{controller=Home}/{action=Index}");

app.MapAreaControllerRoute(
    name: "Client",
    areaName: "Home",
    pattern: "{controller=Home}/{action=ActivateEmail}/{email?}/{password?}");
//app.MapControllerRoute(
//    name: "default",
//    pattern: "{controller=Home}/{action=Index}/{id?}");

app.MapAreaControllerRoute(
    name: "Admin",
    areaName: "Ad",
    pattern: "Ad/{controller=Admin}/{action=Index}");


//app.MapControllerRoute(
//    name: "Areas",
//    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

app.Run();
